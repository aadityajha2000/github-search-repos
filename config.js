module.exports = {
  baseUrl: "https://api.github.com/",
  HEADER_ACCEPT: "application/vnd.github.v3+json",
  RAW_HEADER_ACCEPT: "application/vnd.github.v3.raw",
};

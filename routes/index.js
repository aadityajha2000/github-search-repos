var express = require("express");
var router = express.Router();

// Search repositories
const searchRouter = require("./api/searchRepos");
router.use("/search", searchRouter);

module.exports = router;

const express = require("express");
const router = express.Router();
const searchController = require("../../modules/search_repos/searchReposController");

router.get("/readme", searchController.getReadMeContent);
router.get("/", searchController.search);
module.exports = router;
